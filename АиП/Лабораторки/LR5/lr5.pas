program LR5;

const
  n = 4;

type
  mass = array[1..n, 1..n] of real;

var
  C, D: mass;
  F: text;

procedure vvod(var m: mass);
var
  i, j: integer;
begin
  writeln('x=');
  for i := 1 to n do 
    for j := 1 to n do 
      read(m[i, j]); 
  writeln; 
end;

procedure cht(var m: mass);
var
  i, j: integer;
  F: text;
begin
  assign(F, 'C:\matrix.txt');
  reset(F);
  readln(F);
  for i := 1 to n do 
    for j := 1 to n do 
      read(F, m[i, j]);
  close(F);
  writeln;
end;

procedure sloj(C, D: mass; var m: mass);
var
  i, j: integer;
  f: text;
begin
  assign(f, 'C:\lr5');
  rewrite(f);
  writeln(f, 'C+D = ');
  for i := 1 to n do
  begin
    for j := 1 to n do
    begin
      m[i, j] := C[i, j] + D[i, j];
      write(f, m[i, j]:5:4, ' ');
    end;
    writeln(f);
  end;
  close(f);
end;

function element(var m: mass): real;
var
  i, j, k: integer;
begin
  for i := 2 to n do
  begin
    for j := 1 to i - 1 do
    begin
      if (m[i, j] < 0 and m[i, j]< min) then
      begin
        element := m[i, j];
        break;
      end;
    end;
  end;
  writeln;
end;

begin
  vvod(C);
  cht(D);
  sloj(C, D);
  assign(f, 'C:\lab5');
  append(f);
  writeln(f);
  writeln(f, '� ������� C = ', element(A):5:4);
  writeln(f, '� ������� D = ', element(B):5:4);
  writeln(f, '��������� �������� C,D=', element(C):5:4);
  close(f);
end.