program massiv;

const
  n = 10;

type
  tMass = array [1..n] of real;

var
    m: real;
    i: integer;
    x: tMass;
{*****************************************************}
  function maxval(var mass: tMass; size: Integer): real;
  var
   i: Integer;
    max: Real;
  begin
   max := mass[1];
   for i := 2 to size do
      if mass[i] > max then
       max := mass[i];
   maxval := max;
  end;
{*****************************************************}
  begin
   for i := 1 to n do
   begin
     write('x[', i:2, ']=');
      readln(x[i]);
   end;
    m := maxval(x, n);
    writeln('Max=', m);
   readln;
end.