Program Sentence;{�����������}
  Const max_num_words=127; {������������ ���������� ���� 
                            � �����������}
  Type
    wordsArr = array[1..max_num_words] of string;
  Var
    words:wordsArr; {������ ����}
    n,{����� �����}
    i:byte; {����� ������� � ������}
    source, res:string;{�������� � �������������� �����������}
    
    procedure splitIntoWords(Str:string; var words:wordsArr; 
                             var n:byte; limit:byte);
      {��������� ������ �� �����. ����� ��������� �����
       ��� ����������� ���������}
      var i: byte;
      begin 
         {����������: �������� ������� � ������ � ����� ������:}
          i:=1;
          while (Str[i]=' ') do inc(i);
          delete(Str,1,i-1);
          i:=length(Str);
          while (Str[i]=' ') do dec(i);
          delete(Str,i+1,length(Str)-i);
         {--------------����� ����-------------------}
          n:=1; {������� ���� - ���� ��� �� �����}             
          i:=pos(' ',Str);{����� ����� ������� �����}
          while (i>0) do {���� ���� �����}
            begin
              words[n]:=copy(Str,1,i-1); {��������� �����}
              Inc(n); {����������� ��� ���������� �����}
              if n > limit then break; {���������� ����������� �� ����� ����}
              while (Str[i]=' ') do Inc(i);{������� ������ ��������}
              delete(str,1,i-1);{������� �� ������ ��� 
                                 ������������������ �����}
              i:=pos(' ',Str);{������� ����� ���������� �����}  
            end; //while
           {��������� �����:}
            if n<=limit then words[n]:=copy(Str,1,length(Str))
            else n:=n-1;
     end;
     
  function collectWords(var words:wordsArr; n:byte):String;   
  {������ ����, ������������ � ��������������� �� ����
   � �� �� ����� � ������}
    var
      i:byte;
      S:string;
    begin
      S:='';{������ ������}
      for i:=1 to n do
        if words[i][1]=words[i][length(words[i])]
          then S:=S+' '+words[i];
        delete (S,1,1); {������� ������ � ������ ������}  
      collectWords:=S;
    end;   
  begin
    writeln ('������� ������, � ������� '+
              '����� ��������� ���������');
    readln (source);
    splitIntoWords(source,words,n,max_num_words);
    writeln ('������ ����:');
    for i:=1 to n do writeln (words[i]);
    res:=collectWords(words,n);
    writeln ('�������� �����������:');
    writeln (source);
    writeln ('�������������� �����������:');
    writeln(res);
 end.   
    