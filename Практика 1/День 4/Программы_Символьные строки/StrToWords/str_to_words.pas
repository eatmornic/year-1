Program search_word_in_file;
  Const
    delimiters = [ ' ' , '.' , ',' , ':' , ';' , '!' , '?' ]; 
    {��������� ������������ ����}
    len=80; {������������ ����� ������ �����}
  Var
    in_file : text;
    pattern_word, {��������� �����-�������}
    word_intext, {�����, ��������� � ������}
    stroka :string[len]; {����������� ������ �����}
    k, n, strokalength : word;
    file_name : string[20];
  Begin
    WriteLn( '������� ��� ���������� �����');
    ReadLn(file_name);
    WriteLn('������� ������� ��� ������');
    ReadLn(pattern_word);
    Assign(in_file, file_name);
    Reset(in_file);
    {������ ����� �� ����� �����:}
    n := 0;{����� ������}
    while not Eof(in_file) do
      begin
        Inc(n); {��������� ����� ������}
        ReadLn(in_file, stroka);{���� ������}
        {��������� ����� ������� ������:}
        strokalength := Length(stroka);
        {��������� ����� �� ������� ������}
        k := 1;
        word_intext := '';{������ ������}
        while k <= strokalength do
          begin
            if (stroka[k] in delimiters) 
                or (k=strokalength) 
              then {��������� ����� �����}
                begin 
                  if (k=strokalength)and not(stroka[k]in delimiters)
                    then {��������� ��������� ����� � �����:}
                      word_intext := word_intext+stroka[k];
                  {�������� ���������� � ��������:}
                   if word_intext = pattern_word 
                     then
                       begin
                         WriteLn('����� ', pattern_word,
                         ' ������� � ������ ', n:4, ':');
                         WriteLn(stroka);
                         Break; {�����-������� ����������, 
                                 �������� �����}
                       end;
                  word_intext := '';
                end
              else {���� �� ����� �����, 
                    �� �������� ����� � �����}
                word_intext := word_intext+stroka[k];
            Inc(k);
          end {while k}
      end; {while not eof}
    Close(in_file);
  end.