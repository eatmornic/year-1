Program Demo_TypedFiles;
  Uses Crt; {���������� ������ ���������� ���������� 
           � ��������}
  Type person=record {������ � ����������}
                FIO:string[15]; {������� � ��������}
                Year:integer; {��� ��������}
                Salary:real; {�����}
              End;
  Var 
    f: file of person; {���� � �������� � �����������}
    Elem1,Elem2: person; {��� �������� ����� ���� ������}
    Number:integer;{����� ������ ����}
{===============���������� ������ � ����===================}
  Procedure Insert_in_file;
    Begin
{���������� ��������� � �������, ������������ ��������
�����. ���� ��������� ����� ����, �� � ������. ���� ������ 
����������� � ������������ ����, �� � �����}
      Seek(f,FileSize(f));
      Writeln('������� �������� � �����������.');
      Writeln('��� ������ ������ ������� ����������� ������ *');
      While True Do {����������� ����}
        Begin
          Write('������� ������� � �������� (�����������): ');
          Readln(Elem1.FIO);
          If Elem1.FIO='*' Then Break; {�����, ���� ������� *}
          Write('������� ��� ��������: ');
          Readln(Elem1.Year);
          Write('������� �����: ');
          Readln(Elem1.Salary);
          Write(f,Elem1)
        End
    End;
{==============���������� �����====================}
  Procedure Sort_file; {����� ��������}
    Var i,j:integer;
    Begin
      Seek(f,0); {��������� ����� ���������� � ������}
      For i:=filesize(f)-1 downto 1 do {����, �������� 
                                        ����� ��������}
        For j:=0 to i-1 do {���������� �������� �� 0-�� ��(i-1)-��}
          Begin
            Seek(f,j); {��������� ���������� �� j-�� �������}
            Read(f,Elem1,Elem2); {��������� ��� ������}
            If Elem1.Salary > Elem2.Salary {�������� ������}
              then
                Begin
                  Seek(f,j); {������� ��������� � j-�� �������}
                  Write(f,Elem2,Elem1); {�������� ������� ������}
                End
          End
     End;
{===========����� ����������� �����==============}
  Procedure Print_file;
     Begin
       Seek(f,0); {��������� ����� ���������� � ������}
       Writeln('�������':15, '���':5, '��������':10);
       While not eof(f) do {�������� ���� ������� 
                            �� ����� �����}
          Begin
            Read(f,Elem1);
            Writeln(Elem1.FIO:15, Elem1.Year:5, 
                    Elem1.Salary:10:2);
          End;
       Readkey; {�������� ������� ������� Enter}
     End;
{====����� ����������� � ���������� ��. ������==========}
  Procedure Search_persons;
     Var 
       Found: boolean; {������� ������ ������}
       S: string[15]; {������ � ��������}
       N_persons: integer; {���������� ��������� �����������}
       SummaSalary: Real; {��������� ����� �����������}
     Begin
       N_persons:=0;
       SummaSalary:=0;
       While True Do
          Begin
            Writeln('������� ������� ��� *');
            Readln(s);
            If s='*' Then Break; {���������� ������, ���� ������� *}
            Found:=False;{��� �� ������}
            Seek(f,0); {��������� ����� ���������� � ������}
            {���� �� ����� ����� � ���� �� ������ ���������
             � ��������� �������� ���������� �����}
            While not eof(f) and not Found Do
               Begin
                  Read(f,Elem1); {��������� ��������� ������ �� �����}
                  If (Pos(s,Elem1.FIO)<>0) and {���� �� � FIO ������� s?}
                     (Elem1.FIO[Length(s)+1]=' ') {���� �� ����� ��� ������?}
                    Then
                      Begin
                  {����� �������� � ��������� ����������, ���������� ��������
                   ��������� ����������� � ���������� ���������� ������}
                        Writeln(Elem1.FIO:15, Elem1.Year:5, Elem1.Salary:10:2);
                        N_persons:=N_persons+1;
                        SummaSalary:=SummaSalary+Elem1.Salary;
                        Found:=True; {��������� ������, �������� ����}
                      End;
               End;
           If not Found Then {���� �� ������}
                           Writeln('������ ���������� ���');
         End;
      If N_persons<>0 Then
      Writeln('C������ �����=', SummaSalary/N_persons);
      Readln; {�������� ������� ������� Enter ��� ��������
              ���� ���������}
    End;
{===========�������� ���������====================}
  Begin
     Assign(f,'person.dat'); {���������� ����� �����. � �����. ������}
     If FileExists('person.dat') {� Turbo Pascal - ������� IOResult}
        Then {���� ���� ����������, ��}
           begin
             Reset(f); {������� ���� ��� ������ � ���������� �������}
             writeln('���������� ������� � ������������ ����')
           end  
        else {���� ����� ���, �� ������� ����� ���� � ������� ���}
           begin
             Rewrite(f); {�������� � �������� ����� ��� ������/������}
             writeln('������ � ����� ����');
           end; 
    readln; {�������� ������� ������� Enter }
    {����������� ����, �������������� ����� �� �����
     ������� ����}
    While True Do
       Begin
          ClrScr; { ������� ������ }
          Writeln('1-���������� ������ � ����');
          Writeln('2-���������� �����');
          Writeln('3-����� ����������� �����');
          Writeln('4-����� ����������� � ���������� ��. ������');
          Writeln('5-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(Number);
             Case Number Of {����� ����������� ��������� �� ������}
                1:Insert_in_file; {����� ��������� �������� �����}
                2:begin 
                   Sort_file; { ����� ��������� ���������� �����}
                   Writeln('���� ����������� ������������ '+
                           '�� ����������� �������');
                   readln; {�������� ������� ������� Enter }
                  end; 
                3:Print_file; {����� ��������� ��������� �����}
                4:Search_persons; {����� ��������� ������ ����������}
                5:Exit { ���������� ��������� ������}
             End;
        End;
  End.      
         