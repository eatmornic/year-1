Program student2;
  Label finish;
  Const max_gr_size=30; {� ������ �� ����� 30 ���������}
  Type
    Student=record
              First_name: string[15];
              Last_name: string[15];
              birthday: record
                           day: integer;
                           month: integer;
                           year: integer
                        end;
            end;
    Group = array[1..30] of Student;
    WordsArr = array[1..5] of string;
    LinesArr = array[1..max_gr_size] of string;
  Var
    S: Student; {������ � ��������}
    gr1, gr2: Group; {��� ������������ ������}
    n1,n2:integer; {�������� ������� �����}
    words:WordsArr; {������ ���� ��� ��������� 
            �� ����� ������ ������ � ��������}
    m:byte;{�������� ������ ������� words - �����
            ���� � ������ �������}
    lines:LinesArr;{������, ��������� �� �����}
    i,{���������� �����}
    code:integer;{��� ������ �������������� ������}
        
    procedure splitIntoWords(Str:string; var words:wordsArr; 
                             var n:byte; limit:byte);
      {��������� ������ �� �����. ����� ��������� �����
       ��� ����������� ���������}
      var i: byte;
      begin 
         {����������: �������� ������� � ������ � ����� ������:}
          i:=1;
          while (Str[i]=' ') do inc(i);
          delete(Str,1,i-1);
          i:=length(Str);
          while (Str[i]=' ') do dec(i);
          delete(Str,i+1,length(Str)-i);
         {--------------����� ����-------------------}
          n:=1; {������� ���� - ���� ��� �� �����}             
          i:=pos(' ',Str);{����� ����� ������� �����}
          while (i>0) do {���� ���� �����}
            begin
              words[n]:=copy(Str,1,i-1); {��������� �����}
              Inc(n); {����������� ��� ���������� �����}
              if n > limit then break; {���������� ����������� �� ����� ����}
              while (Str[i]=' ') do Inc(i);{������� ������ ��������}
              delete(str,1,i-1);{������� �� ������ ��� 
                                 ������������������ �����}
              i:=pos(' ',Str);{������� ����� ���������� �����}  
            end; //while
           {��������� �����:}
            if n<=limit then words[n]:=copy(Str,1,length(Str))
            else n:=n-1;
     end;
  procedure inputarr(var X:LinesArr; var k:integer);
  {���� ����� �� ���������� ����� � ������� 
   ����� ��������� �����}
       var i: integer;
           f: text;
           file_name: string;
       begin
         writeln ('������� ��� ����� �����:');
         readln(file_name);
         assign(f, file_name);
         reset (f);
         k:=0; {������� �����}
         while not eof(f) do
            begin
               k:=k+1;
               readln(f, X[k]);
            end;
         close (f);  
       end;
       
    function how_in_year(var gr:group; n,year:integer):integer;
      {������� ���������� ���������� ��������� �� 
       ������ gr �������� n,���������� � year ����}
       var count,i:integer;
       begin
          for i:=1 to n do
            if gr[i].birthday.year=year
               then inc(count);
          how_in_year:=count;
       end;
           
  begin
    Writeln ('���/�-11-�:');
    inputarr(lines,n1);{���� ����� �� ����� � ������ lines}
    for i:=1 to n1 do {��� ������ ������}
       begin
         splitIntoWords(lines[i],words,m,5);{������� �� �����}
         if m<5 then {���� � ������ ������ 5}
                  begin
                    writeln ('������ � ������ ����� ������');
                    goto finish;{��������� ���������}

                  end;  
         gr1[i].first_name:=words[1];
         gr1[i].last_name:=words[2];
         val(words[3],gr1[i].birthday.day,code);{��������������}
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
           
         val(words[4],gr1[i].birthday.month,code);{��������������}
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
         val(words[5],gr1[i].birthday.year,code);{��������������}
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
       end;
    Writeln ('���/�-12-�:');
    inputarr(lines,n2);
    for i:=1 to n2 do
       begin
         splitIntoWords(lines[i],words,m,5);
         if m<5  then 
                   begin
                     writeln ('������ � ������ ����� ������');
                     goto finish;
                   end;  
         gr2[i].first_name:=words[1];
         gr2[i].last_name:=words[2];
         val(words[3],gr2[i].birthday.day,code);
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
         val(words[4],gr2[i].birthday.month,code);
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
         val(words[5],gr2[i].birthday.year,code);
         if code<>0 then
                      begin
                        writeln ('������ �������������� ������');
                        goto finish;
                      end;  
       end;
     writeln ('� ������ ���/�-11-� ��������� 1998 ���� ��������: ',
                 how_in_year(gr1,n1,1998));
     writeln ('� ������ ���/�-11-� ��������� 1999 ���� ��������: ',
                 how_in_year(gr1,n1,1999));
     writeln ('� ������ ���/�-12-� ��������� 1998 ���� ��������: ',
                 how_in_year(gr2,n2,1998));
     writeln ('� ������ ���/�-12-� ��������� 1999 ���� ��������: ',
                 how_in_year(gr2,n2,1999));
finish:               
  end.              
    
       
         