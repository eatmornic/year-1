program StringQueue;
  Uses Crt; {���������� ������ ���������� ���������� 
           � ��������}
  Type
     ListPoint = ^ListEl;  {������ �� ������� ������}
     ListEl = record
                 inf:string;
                 next:ListPoint;
                 prev:ListPoint;
              end;   
                
   Var
      q:listPoint;{��������� �� �������, ����������
                   �������-������� �����}
      Number: integer;{����� ������ ����}
      str:string; {������, ����������� � �������}
      
  procedure push(str:string);
   {�������� ������� � ������� (� �����)}
    var e:ListPoint;
    begin
      new (e); {������� ����� �������}
      e^.inf:=str; {�������� ������}
      if q^.next=nil {������ ����?}
         then begin q^.next:=e; q^.prev:=e end
         else
           begin
            q^.prev^.next:=e;
            e^.prev:=q^.prev;
            q^.prev:=e;
           end
    end;
  function pop:string;
    {������� ������� �� ������� (�� ������)}
    var str:integer;
    begin
      if q^.next=nil {������ ����?}
        then pop:='������� �����' 
        else 
          begin
            pop:=q^.next^.inf;
            if q^.next=q^.prev {� ������ ���� �������}
              then
                 begin
                   q^.prev:=nil;
                   dispose(q^.next)
                 end
              else
                 begin
                   q^.next:=q^.next^.next;
                   dispose(q^.next^.prev);
                 end                  
          end
   end;
 procedure putQueueForward;
   {������� ���������� ������� �� ������ � ������}
   var e:ListPoint;
   begin
     e:=q^.next;
     writeln ('���������� ������� �� ������ � ������:');
     while e<>nil do 
        begin
          writeln (e^.inf);
          e:=e^.next
        end
  end; 
   procedure putQueueBackward;
   {������� ���������� ������� �� ������ � ������}
   var e:ListPoint;
   begin
     e:=q^.prev;
     writeln ('���������� ������� �� ������ � ������:');
     while e<>nil do 
        begin
          writeln (e^.inf);
          e:=e^.prev
        end
  end;

 begin
      new (q); {������� ������-����� �������, 
                ���� ������� ��� �����}
      While True Do
        Begin
          ClrScr; { ������� ������ }
          Writeln('1-���������� �������� � �������');
          Writeln('2-���������� �������� �� �������');
          Writeln('3-����� ����������� ������� �� ������ � ������');
          Writeln('4-����� ����������� ������� �� ������ � ������');
          Writeln('5-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(Number);
          Case Number Of {����� ����������� ��������� �� ������}
                1: begin
                      write ('������� ��� ����������: ');
                      readln (str);
                      push(str);
                      writeln('� ������� ������� ������� ',str);
                   end;       
                2: writeln ('���������� �������� �� �������: ',pop);                      
                3: putQueueForward;
                4: putQueueBackward;
                5: Exit { ���������� ��������� ������}
             End;
             writeln('������� <Enter>');
             readln; {������������� �������� �� ����� �������}
        End
    end.
               
            
             
                  
      