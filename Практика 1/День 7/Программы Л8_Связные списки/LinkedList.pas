Program DinLinkedStud; {����������� �������� ������}
  Uses Crt; {���������� ������ ���������� ���������� 
           � ��������}
  Type
     Date = record
                day: integer; {����}
                month: integer; {�����}
                year: integer {���}
            end;
     Student = record
                 id:integer; {����� �������� ������}
                 name: string[15];{������� �.�.}
                 birthday: Date; {���� ��������}
                 ball:real {���� �� ��������� ������}
               end;
     ListPoint = ^ListEl;  {������ �� ������� ������}          
     ListEl = record
                 inf:Student;
                 next:ListPoint;
              end;   
                
   Var
      id:integer;{����� �������� ������ ��������}
      stud:Student; {�������}
      gr:ListPoint;{������������ ������}
      Number: integer;{����� ������ ����}
           
   function addStud (var gr:ListPoint; stud:Student):boolean;
      {���������� �������� stud � ������ gr (� ������ ������);
       gr - ��������� �� ������ ������ ������}
       var el,current:ListPoint;
       begin
        {����������� �� ��������� ���������� ��������� � ������ 
         ������ ���}
         if gr=nil {���� ������ �����}
           then
              begin {������� ������� ������ � ������ ��� 
                     ������� ������}
                new(gr); gr^.inf:=stud; addstud:=true; exit;
              end;
         {����� ������� � ����� id ��� ���� � ������?}
          current:=gr;
          while current<>nil do
             begin
              if current^.inf.id=stud.id
                 then begin
                         addStud:=false;
                         exit
                      end;
              current:=current^.next;
             end;         
         {��� � ������� - ��������� �������� � ������ ������!}       
         new(el); {������� ������ �������� � ������������ ������}
         el^.inf:=stud;{��������� ���� �������� -
                       �������� ���������� ������� �������}
         el^.next:=gr;{��������� ������� ���� ��������� �� �������,
                            ������� ������ ��� ������� ������}  
         gr:=el;{������� ������ ���� ��������� �������}
         addStud:=true {������� ������� ��������}
       end;
      
       
     function delStud (var gr:ListPoint; id:integer):boolean;
       {���������� �������� �� ������ gr �� id;
         gr - ��������� �� ������ ������ ������}
       var current,temp:ListPoint;
       begin
          if gr=nil then begin delstud:=false; exit end;{������ �����}
          if gr^.inf.id=id {������ ������� �������� ����������� �������� 
                            �� �����,�.�. � ���� ��� ���������������}
            then begin
                   temp:=gr;
                   gr:=gr^.next;
                   dispose(temp);
                   delStud:=true;
                   exit
                 end;
          current:=gr;
          while current^.next<>nil do
            begin
              if current^.next^.inf.id = id 
                 then
                   begin
                     temp:=current^.next;
                     current^.next:=current^.next^.next;{������� �� ������}
                     dispose(temp); {����������� ������}
                     delStud:=true;
                     exit
                   end;
             current:=current^.next;      
           end;     
           delStud:=false; {������� �� ������}
       end;
     function avgBall (var gr:ListPoint):real;
        {���������� �������� ����� �� ������;
         gr - ��������� �� ������ ������ ������}
        var current:ListPoint;
            n:integer;
            res:real;
        begin
           current:=gr; n:=0;
           while current<>nil do
              begin
                res:=res+current^.inf.ball;
                n:=n+1;
                current:=current^.next;
              end;                
            avgBall:=res/n
        end;
        
     procedure sortBallDesc (var gr: listPoint);
       {���������� ��������� � ������ �� �������� ����� -
        ����� �������� 2}
       var
         el,old: ListPoint; flag:boolean;
       function swap:boolean;
         {el ��������� �� �������, �������������� ������������� ���� ���������}
          begin {swap} 
          {�������� � �����������, 
          ���� ������� �������� � ������ �� ������� ������:}
             if el^.next^.inf.ball<el^.next^.next^.inf.ball
               then
                 begin
                   old:=el^.next;
                   el^.next:=old^.next;
                   el:=old^.next^.next;
                   old^.next^.next:=old;
                   old^.next:=el;
                   swap:=true; {������������}
                 end 
               else swap:=false; 
          end; {swap}       
               
     begin {sortBallDesc}
         if gr=nil then exit {������ ����}
           else if gr^.next=nil then exit;{� ������ ����� ���� �������}
         flag:=false;
         while not flag do {���� ������ �� ������������}
           begin
             flag:=true; {��� �� - ��������}
             el:=gr;
             {������ ���� ��������� ������������� �������� (�� �����), �.�.
              �� ����� ��������������� ��������}
             if gr^.inf.ball<gr^.next^.inf.ball
               then
                 begin
                   gr:=el^.next;
                   old:=gr^.next;
                   gr^.next:=el;
                   el^.next:=old;
                   flag:=false; {���� ������������, ������, ������ �� ����������}
                 end
               else 
                while el^.next^.next<>nil do {���������� �������� ������}
                  begin
                     if swap {���� ������������}
                      then
                        begin
                          flag:=false;                      
                          break;{��������� ����� �� while}
                        end;  
                     el:=el^.next;
                 end;
           end
    end;
    
    function size (var gr: listPoint):integer;
      {���������� ����� ��������� � ������}
        var current:ListPoint;
            n:integer;
        begin
          n:=0;current:=gr;
          while current<>nil do
              begin
                n:=n+1;
                current:=current^.next;
              end;
          size:=n;
        end;  
       
     
     procedure inputStud (var stud:Student);
       {���� ������ � ����� ��������}
       begin
         writeln('���� ������ � ��������:');
         write ('����� �������� ������: '); readln(stud.id);
         write ('������� � ��������: '); readln(stud.name);
         writeln ('���� ��������: ');
         write ('����: '); readln(stud.birthday.day);
         write ('�����: '); readln(stud.birthday.month);
         write ('���: '); readln(stud.birthday.year);
         write ('����: '); readln(stud.ball);
       end;
       
       procedure putGroup (var gr: listPoint);
         {����� ������ ������}
         var current:ListPoint;
         begin
           writeln ('������ ������:');
           current:=gr;
           while current<>nil do
              begin
                writeln (current^.inf.id:10, ' ',current^.inf.name:15, '    ',
                         current^.inf.birthday.day:2, '.',
                         current^.inf.birthday.month:2,'.',
                         current^.inf.birthday.year:4, '    ', 
                         current^.inf.ball:4:2);
                         current:=current^.next;
              end         
         end;              
           
    begin
      gr:=nil; {���� ������ �����}
      While True Do
        Begin
          ClrScr; { ������� ������ }
          Writeln('1-���������� �������� � ������');
          Writeln('2-���������� �������� �� ������');
          Writeln('3-����� ������ ������');
          Writeln('4-���������� �������� ����� �� ������');
          Writeln('5-���������� ��������� �� �������� �����');
          Writeln('6-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(Number);
          Case Number Of {����� ����������� ��������� �� ������}
                1: begin
                      inputStud (stud);
                      if addStud (gr,stud) 
                         then writeln ('������� ������� ��������.')
                         else writeln('�� ������� �������� ��������',
                           ' - ��������� ����������� ������������ id.');
                   end;       
                2: begin 
                       write('������� ����� �������� ������ ��������: ');
                       readln (id);
                       if delStud (gr,id) 
                           then writeln ('������� ������� ��������.')                                             
                           else writeln ('�� ������� ��������� ��������',
                                         ' - ������� �� ������.')  
                   end;        
                3: begin
                      writeln ('� ������ ���/�-11-� ',size(gr),' ���������.');
                      putGroup (gr);
                   end;   
                4: Writeln ('������� ���� �� ������: ', avgBall(gr):5:2);
                5: begin
                     sortBallDesc(gr);
                     writeln('��������� ���������� ��������� �� �������� �����');
                   end;  
                6:Exit { ���������� ��������� ������}
             End;
             writeln('������� <Enter>');
             readln; {������������� �������� �� ����� �������}
        End
    end.
          
    
