program intStack;
  Uses Crt; {���������� ������ ���������� ���������� 
           � ��������}
  Type
     ListPoint = ^ListEl;  {������ �� ������� ������}
     ListEl = record
                 inf:integer;
                 next:ListPoint;
              end;   
                
   Var
      head:listPoint;{��������� �����}
      Number: integer;{����� ������ ����}
      n:integer; {�����, ����������� � �����}
      
  procedure push(n:integer);
   {�������� ������� � ����
    push in (����.) - ���������}
    var el,old:ListPoint;
    begin
      new (el); {������� ����� �������}
      el^.inf:=n; {�������� ��������}
      if head=nil {������ ����?}
         then head:=el
         else
           begin
             old:=head;
             head:=el;
             head^.next:=old;
           end
    end;
  function pop:integer;
    {������� ������� �� �����
    pop out (����.) - ���������}
    var el:ListPoint;
        n:integer;
    begin
      if head=nil {������ ����?}
        then pop:=-32768
        else 
          begin
            pop:=head^.inf;
            el:=head;
            head:=head^.next;
            dispose(el);
          end
   end;
 procedure putStack;
   {������� ���������� �����}
   var el:ListPoint;
   begin
     el:=head;
     writeln ('���������� �����:');
     while el<>nil do 
        begin
          writeln (el^.inf);
          el:=el^.next
        end
  end;      

 begin
      head:=nil; {���� ���� ����}
      While True Do
        Begin
          ClrScr; { ������� ������ }
          Writeln('1-���������� �������� � ����');
          Writeln('2-���������� �������� �� �����');
          Writeln('3-����� ����������� �����');
          Writeln('4-�����');
          Writeln('-------------------------------');
          Writeln('������� ����� ������ ����');
          Readln(Number);
          Case Number Of {����� ����������� ��������� �� ������}
                1: begin
                      write ('������� ����� � ��������� -32767..32767: ');
                      readln (n);
                      push(n);
                      writeln('� ���� ������� ������� ',n);
                   end;       
                2: begin
                    n:=pop;
                    if n=-32768
                      then writeln ('���� ����')
                      else writeln ('�� ����� �������� ������� �� ���������: ',n);
                   end;   
                3: putStack;
                4:Exit { ���������� ��������� ������}
             End;
             writeln('������� <Enter>');
             readln; {������������� �������� �� ����� �������}
        End
    end.
               
            
             
                  
      