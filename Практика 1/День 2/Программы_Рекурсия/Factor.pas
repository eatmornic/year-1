program Factorial;
    var
     i:integer;

  function fact_r(n: integer):longint;
   {n! - ����������� �����}
    var
     f: longint;
    begin
     f:=1;
     if (n>1) then f:=n*fact_r(n-1);
     fact_r:=f;
    end;    

  function fact_i(n: integer):longint;
   {n! - ������������ �����}
    var
     f: longint;
     i: integer;
    begin
     f:=1;
     for i:=2 to n do f:=f*i;
     fact_i:=f;
    end;    
 
begin
  for i:=1 to 15 do
     writeln (i,'!= ', fact_r(i),' ',fact_i(i));
end.