program Minimum;
    const n=100;
    type
      tarr=array[1..n] of integer;
    var
     A:tarr;
     i,m:integer;

  function arr_min_r(var B:tarr;  first, last:integer):integer;
   {���������� �������� ����� ��������� ������� c �������� �� first �� last - 
    ����������� �����}
    var
     res, tempmin: integer;
    begin
     res:=0; // ���� first > last, ����� ������ 0
     if first<=last //���� ����, ��� ���������
       then if first=last 
              then res:=B[first] // ������������� �����
              else //����������� �����:
                begin
                  tempmin:=arr_min_r(B,first+1,last);
                  if B[first]<tempmin
                    then res:=B[first]
                    else res:=tempmin
                end;    
     arr_min_r:=res;
    end;  
   
  function arr_min_i(var B:tarr;  first, last:integer):integer;
   {{���������� �������� �����  ��������� ������� c �������� �� first �� last - 
    ������������ �����}
    var
     res,i: integer;
    begin
     res:=0; // ���� first > last, ����� ������ 0
     if first<=last //���� ����, ��� ���������
       then 
         begin
            res:=B[first];
            for i:=first+1 to last do 
               if B[i]<res then res:=B[i]
         end;      
      arr_min_i:=res;
    end;    
  begin      
       m:=8;{� ������� ����� 10 ���������}
       A[1]:=10; A[2]:=3; A[3]:=5; A[4]:=1; 
       A[5]:=6; A[6]:=4; A[7]:=2; A[8]:=8;
       writeln('������ �������� �������:');
       for i:=1 to m do 
           write(' ',A[i]); //����� �������� ��������
       writeln;    
       writeln ('Min = ',arr_min_r(A,1,m),
                ' = ',arr_min_i(A,1,m));
  end.