program Summa;
    const n=100;
    type
      tarr=array[1..n] of integer;
    var
     A:tarr;
     i,m:integer;

  function arr_sum_r(var B:tarr;  first, last:integer):integer;
   {����� ��������� ������� c �������� �� first �� last - 
    ����������� �����}
    var
     res: integer;
    begin
     res:=0; // ���� first > last, ����� ������ 0
     if first<=last //���� ����, ��� �����������
       then if first=last 
              then res:=B[first] // ������������� �����
              else //����������� �����:
                 res:=B[first]+arr_sum_r(B,first+1,last);
     arr_sum_r:=res;
    end;    

  function arr_sum_i(var B:tarr;  first, last:integer):integer;
   {����� ��������� ������� c �������� �� first �� last - 
    ������������ �����}
    var
     res,i: integer;
    begin
     res:=0; // ���� first > last, ����� ������ 0
     for i:=first to last do res:=res+B[i];
     arr_sum_i:=res;
    end;    
  

begin
       m:=10;{� ������� ����� 10 ���������}
       writeln('������ �������� �������:');
       for i:=1 to m do 
         begin 
           A[i]:=i+1; // ���������� �������
           write(' ',A[i]); //����� �������� ��������
         end;    
       writeln;    
       writeln ('Sum= ',arr_sum_r(A,1,m),' = ',arr_sum_i(A,1,m));
end.