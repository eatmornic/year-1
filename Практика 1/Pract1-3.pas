program pract_pas;

{uses 
CRT;}

type
  matrix = array[0..49, 0..59] of byte;
  vector = array[0..49] of byte;

var
  n, m{, e}: integer;            //n - строки, m - столбцы
  C: Matrix;
  D: Vector;
  
  E: vector;

{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~1~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}
{Процедура ввода матрицы из файла}
procedure input(var ma: matrix);
var
  F: text;
  i, j: integer;
begin
  assign(F, 'Z:\Desktop\matrix2.txt');
  reset(F);
  readln(F, n, m);
  for i := 1 to n do 
  begin
    for j := 1 to m do 
      read(F, ma[i, j]); 
  end;
  close(F);
end;
{--------------------------------}


{Процедура вывода матрицы в окно терминала}
procedure output(var ma: matrix);
var
  F: text;
  i, j: integer;
begin
  assign(F, 'Z:\Desktop\matrix2.txt');
  reset(F);
  for i := 1 to n do
  begin
    for j := 1 to m do
      write(ma[i, j]:3);
    writeln;
  end;
  writeln;
  close(F);
end;
{-----------------------------------------}


{Процедура вывода вектора в окно терминала}
procedure vectoroutput(var ve: vector);
var
  F: text;
  i: integer;
begin
  assign(F, 'Z:\Desktop\matrix2.txt');
  reset(F);
  readln(F, n);
  for i := 1 to m do 
  begin
    read(F, ve[i]);
    write(' ', ve[i]); 
  end;
  writeln;
  
  close(f);
  
end;
{------------------------------------------}


{Процедура выводящая максимальный элемент в строке}
procedure max(var ma: matrix; var ve: vector);
var
  i, j: integer;
begin
  for i := 1 to n do
  begin
    ve[i] := ma[i, 1];
    for j := 2 to m do
      if ma[i, j] > ve[i] then ve[i] := ma[i, j];
  end;
end;
{-------------------------------------------}


{Вывод значения строк}
procedure outputarr(var ve: vector);
var
  i: integer;
begin
  for i := 1 to n do
    write(' ', ve[i]);
  writeln;
  writeln;
end;
{--------------------}

{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~2~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}

{Сортировка}
procedure sort(var ve: vector; n: integer);
var
  x: byte;
  i, j: integer;
begin
  
  for i := 0 to m do
  
  begin
    x := ve[i];
    j := i - 1;
    while j >= 0 do
    begin
      if x < ve[j] 
        then ve[j + 1] := ve[j]
      else break;
      j := j - 1;
    end;
    ve[j + 1] := x;  
  end; 
end;
{------------}

{вывод отсортированного}
procedure outputvectsort(var ve: vector);
var
  i: integer;
begin
  for i := 1 to m do
    write(' ', ve[i]);
  writeln;
  
end;
{-----------------------}

{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~3~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}

{Функция произвдения отрицательных(?) элементов - рекурсия}
function product_rec(var e: vector; i, n: integer): integer;
begin
  if i = n - 1 then product_Rec := 1
  else product_rec := e[n] * product_rec(e, i, n + 1);
end;
{------------------------------------------------------}

{Функция произвдения положительных элементов - итерации}
function product_iter(var e: vector; n: integer): integer;
var
  res, i: integer;
begin
  res := 1;
  for i := 1 to n do res := res * e[i];
  product_iter := res;
end;

{~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~}

begin
  {1}
  writeln('Задание №1');
  input(C);
  writeln('Вывод матрицы на окно терминала: ');
  output(c);
  writeln('Вывод вектора на окно терминала: ');
  vectoroutput(D);
  writeln;
  writeln('Вывод максимальных значений строк матрицы: ');
  max(C, D);
  outputarr(D);
  
  
  {2 (Метод прямых вставок. По возростанию)}
  writeln('Задание №2');
  write('Вектор до сортировки: ');
  vectoroutput(D);
  write('После сортировки: ');
  Sort(D, n);
  outputvectsort(D);
  
  
  
  {3 (рекурсивная: произведение отрицательных; итерационная:  произведение положительных)}
  writeln;
  writeln('Задание №3');
  
  
  writeln;
  writeln(product_rec(d, n, 1), ' ', product_iter(d, n))
  
end.