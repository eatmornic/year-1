program sort1;
  type
    tarr1 = array [0..99] of integer;
  var
    A,B: tarr1;
    n,i: integer;
    
  
 procedure sort_direct_insertion(var A: tarr1; n: integer);
 {����� ������ �������}
    var
      x, i, j: integer;
    begin
      for i:=1 to n-1 do
        begin
          x:=A[i];
          j:=i-1;
          while j>=0 do
            begin
              if x<A[j]
                then A[j+1]:=A[j]
                else break;
              j:=j-1; 
            end;
          {��� Pascal ���� for j:=i-1 downto 0 do... ������������ ������,
          �.�.��� ������ �� ����� �������� j �������� ������ 0, � �� -1 
          (�����������������), � ������ ������ ��� ���������}
          A[j+1]:=x;
        end
    end; 
    
    

begin {�������� ���������}
  write('������� ����� ��. ������� n=');
  readln(n);
  writeln('������� �������� ������� �');
  inputarr(A,n);
  writeln('������� �������� ������� B');
  inputarr(B,n);
  writeln('�� ����������');
  putarr(A,n);
  putarr(B,n);
  sort_direct_insertion(A,n);
  sort_direct_insertion(B,n);
  writeln('����� ����������');
  putarr(A,n);
  putarr(B,n);
end.